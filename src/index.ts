export function fibonacci(num: number): number | null {
  if (num < 0) {
    return null;
  }

  let a = 1, b = 0, temp;

  while (num >= 0){
    temp = a;
    a = a + b;
    b = temp;
    num--;
  }
  
  return b;
}