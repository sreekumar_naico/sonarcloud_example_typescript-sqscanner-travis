import { fibonacci } from "../src/index"

it("should return '1' for 1", () => {
  expect(fibonacci(1)).toBe(1);
})

it("should return '1' for 0", () => {
  expect(fibonacci(0)).toBe(1);
})

it("should return 'null' for -1", () => {
  expect(fibonacci(-1)).toBe(null);
})

it("should return '5' for 4", () => {
  expect(fibonacci(-1)).toBe(null);
})